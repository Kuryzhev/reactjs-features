var gulp = require("gulp"); // Good old gulp
var transform = require("vinyl-transform"); // Transform browserify stream
var source = require("vinyl-source-stream"); // Output for bundles
var browserify = require("browserify"); // Bundling tool
var babelify = require("babelify"); // Compile JSX and ES2015
var _ = require("lodash"); // Used to package.json keys
var connect = require("gulp-connect"); // Runs a local dev server
//var open = require("gulp-open"); // Open a URL in a web browser

/*gulp.task("connect", function() {
    connect.server({
        root: ["dist"],
        port: 8080,
        base: "http://localhost",
        livereload: true
    });
});

gulp.task("open", ["connect"], function() {
    gulp.src("dist/index.html")
        .pipe(open({
            uri: "http://localhost:8080/",
            "app": "chrome"
        }));
});*/

gulp.task("copy", function() {
    var c = gulp.src("src/index.html")
        .pipe(gulp.dest("dist/"));
    //reload(c);
});

gulp.task("vendor", function() {
    var npmPackages = getNPMPackages();
    return browserify()
        .require(npmPackages)
        .bundle()
        .on("error", handleError)
        .pipe(source("vendors.js"))
        .pipe(gulp.dest("./dist/js/"));
});

gulp.task("bundle", function() {
    var npmPackages = getNPMPackages();

    var b = browserify("./app/index.jsx")
        .external(npmPackages)
        .transform(babelify)
        .bundle()
        .on("error", handleError)
        .pipe(source("bundle.js"))
        .pipe(gulp.dest("./dist/js/"));

    //reload(b);
});

gulp.task("default", ["vendor", "copy", "bundle"]);

gulp.task("watch", ["default"], function() {
    gulp.watch(["app/*.jsx","app/**/*.jsx"], ["bundle"]);
    //gulp.watch("src/*.html", ["copy"]);
});

// Helper functions
function getNPMPackages() {
    var packageManifest = {};
    try {
        packageManifest = require("./package.json");
    } catch (e) {}

    return _.keys(packageManifest.dependencies) || [];
}

function handleError() {
    var args = Array.prototype.slice.call(arguments);
    console.log("errors: ", args);
    this.emit("end");
}

//function reload(stream) {
//    if (connect && connect.reload) {
//        stream.pipe(connect.reload());
//    }
//}