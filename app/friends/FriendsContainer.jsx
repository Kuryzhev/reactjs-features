/**
 * Created by user1 on 22.06.2016.
 */
import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'

import AddFriend from './AddFriend.jsx'
import FriendsList from './FriendsList.jsx'

export default class FriendsContainer extends Component {
    constructor() {
        super();
        this.state = {
            name: 'Main',
            friendList: ['alice', 'clark', 'vasya']
        };

        this.addFriend = this.addFriend.bind(this);
    }

    addFriend(newFrined) {
        this.setState({friendList: this.state.friendList.concat([newFrined])});
    }

    render() {
        return (
            <div>
                <FriendsList friends={this.state.friendList}/>
                <AddFriend addFriend={this.addFriend}/>
            </div>
        )
    }
}