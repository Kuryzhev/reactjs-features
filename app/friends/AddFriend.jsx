import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'

export default class AddFriend extends Component {
    constructor() {
        super();
        this.state = {
            newFriend: ''
        }
        this.addFriendChange = this.addFriendChange.bind(this);
        this.addFriendClick = this.addFriendClick.bind(this);
    }

    addFriendChange(e) {
        this.setState({newFriend: e.target.value});
    }

    addFriendClick() {
        this.props.addFriend(this.state.newFriend);
        this.setState({newFriend: ''});
    }

    render() {
        return (
            <div>
                <input type="text" value={this.state.newFriend} onChange={this.addFriendChange}/>
                <button onClick={this.addFriendClick}>Add Friend</button>
            </div>
        )
    }
}