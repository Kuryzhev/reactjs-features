import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'

export default class FriendsList extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <ul>
                {this.props.friends.map(function(name){
                    return <li>{name}</li>
                    })}
            </ul>
        )
    }
}
FriendsList.defaultProps = {
    friends: [1, 2, 3]
};