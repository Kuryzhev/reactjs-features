import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { Link,browserHistory } from 'react-router'
export default class MainLayout extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div className="app">
                <header className="primary-header"></header>
                <aside className="primary-aside">
                    <ul>
                        <li><Link to="/home">Home</Link></li>
                        <li><Link to="/friends">Users</Link></li>
                    </ul>
                </aside>
                <main>
                    {this.props.children}
                </main>
            </div>
        )
    }
}