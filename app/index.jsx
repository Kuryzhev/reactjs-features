import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import FriendsContainer from './friends/FriendsContainer.jsx'
import MainLayout from './MainLayout.jsx'
import HomeContainer from './home/HomeContainer.jsx'

import { Router, Route, Link, IndexRoute, browserHistory } from 'react-router'


ReactDOM.render((
        <Router>
            <Route path="/" component={MainLayout}>
                <IndexRoute component={HomeContainer}/>
                <Route path="/home" component={HomeContainer}></Route>
                <Route path="/friends" component={FriendsContainer}></Route>
            </Route>
        </Router>
    ),
    //<FriendsContainer />,

    document.getElementById('app')
);